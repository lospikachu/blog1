package com.captton.blogs;

public class Comentarios {

	private String mensaje;
	private String user;
	private Post posteo;
	
	public Comentarios() {
		
	}
	
	public Comentarios(String user,Post posteo, String mensaje) {
		this.user = user;
		this.posteo = posteo;
		this.mensaje = mensaje;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getUser() {            
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Post getPosteo() {
		return posteo;
	}

	public void setPosteo(Post posteo) {
		this.posteo = posteo;
	}
	
	
}
