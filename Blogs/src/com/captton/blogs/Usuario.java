package com.captton.blogs;



public class Usuario {

	private String nombre;
	private String eMail;
	private Comentarios comentario;
	// private Post post;
	// private ArrayList<Comentarios> comentariosDelUsuario;
	// ArrayList<Post> postsDelUsuario;//da error al comentar hay que revisar el
	// hecho de que hayan dos variables post
	//private ArrayList<Post> posts;

	public Usuario(String nombre, String eMail) {

		this.nombre = nombre;
		this.eMail = eMail;
		// this.comentariosDelUsuario = new ArrayList<Comentarios>();
		// this.postsDelUsuario = new ArrayList<Post>();
		//this.posts = new ArrayList<Post>();
	}

	// titulo, contenido
	public void crearPost(String titulo, String contenido,Blogs blog) {
		blog.AgregarPost(new Post(titulo, this, contenido));
		// postsDelUsuario.add(new Post(titulo, this, contenido));
		//posts.add(new Post(titulo, this, contenido));
	}

	// usuaio, post
	/*
	 * public void comentar(Usuario user, String titulo, String mensaje) { //
	 * comentariosDelUsuario.add(new Comentarios(nombre, post, mensaje));
	 * this.buscarPost(titulo);//.setComentarios(new Comentarios(nombre, post,
	 * mensaje)); // post.setComentarios(new Comentarios(nombre, post, mensaje)); }
	 */

	/*public void listarPost() {
		// for (Post post : postsDelUsuario) {
		for (Post post : posts) {
			System.out.println("Titulo de post: " + post.getTitulo() + "Mensaje: " + post.getContenido() + "Usuario: "
					+ this.nombre + "\n");

		}

	}*/
/*
	public void comentarMiPost(String titulo, Comentarios comentario, String mensaje) {

		for (int i = 0; i < posts.size(); i++) {
			if (posts.get(i).getTitulo().equals(titulo)) {
				posts.get(i).setComentarios(new Comentarios(nombre, posts.get(i), mensaje));
			} else {
				System.out.println("No se encontro el post " + titulo);
			}
		}
	}*/

	public void comentarPost(Usuario user, String titulo,  String mensaje,Blogs blog) {

		blog.setearComentarios(user, titulo, mensaje);
		
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public Comentarios getComentario() {
		return comentario;
	}

	public void setComentario(Comentarios comentario) {
		this.comentario = comentario;
	}
	/*
	 * public Post getPost() { return post; } public void setPost(Post post) {
	 * this.post = post; } public ArrayList<Comentarios> getComentariosDelUsuario()
	 * { return comentariosDelUsuario; } public void
	 * setComentariosDelUsuario(ArrayList<Comentarios> comentariosDelUsuario) {
	 * this.comentariosDelUsuario = comentariosDelUsuario; } public ArrayList<Post>
	 * getPostsDelUsuario() { return postsDelUsuario; } public void
	 * setPostsDelUsuario(ArrayList<Post> postsDelUsuario) { this.postsDelUsuario =
	 * postsDelUsuario; }
	 */

}
