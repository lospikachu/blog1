package com.captton.blogs;

import java.util.ArrayList;

public class Post {
	private String titulo;
	private Usuario usuario;
	private String contenido;
	private ArrayList<Comentarios> comentarios;
	
	public Post(String titulo, Usuario usuario, String contenido) {
		super();
		this.titulo = titulo;
		this.usuario = usuario;
		this.contenido = contenido;
		this.comentarios = new ArrayList<Comentarios>();
	}
	
	public void getComentarios () {                 
		for(Comentarios com:comentarios) {
			System.out.println(com.getUser()+"dice: "+com.getMensaje()+"\n");
		}
	}
	public void setComentarios(Comentarios comentario) {
		comentarios.add(comentario);
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public String getContenido() {
		return contenido;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	
	
}
